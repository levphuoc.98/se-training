<?php

class Fields_View_Helper_FieldPhoneNumber extends Fields_View_Helper_FieldAbstract {

    public function fieldPhoneNumber($subject, $field, $value) {
        return substr($value->value, 0, 3) . ' ' . substr($value->value, 3, 4) . ' ' . substr($value->value, 7);
    }

}
