<?php

/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Website.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */
class Fields_Form_Element_PhoneNumber extends Engine_Form_Element_Text {

    public function init() {
        $this->addValidator('Regex', true, array(
            '/(0[2|3|5|7|8|9])+([0-9]{8})\b/'
        ));
        // Fix messages
        $this->getValidator('Regex')->setMessage("'%value%' is not a valid phone number of Viet Nam.", 'regexNotMatch');
    }
    
    public function setValue($value) {
        parent::setValue($value);
    }

}
