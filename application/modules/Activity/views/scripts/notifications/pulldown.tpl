<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Activity
 * @copyright  Copyright 2006-2020 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: pulldown.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<?php foreach( $this->notifications as $notification ): ?>
  <li<?php if( !$notification->read ): ?> class="notifications_unread"<?php endif; ?> value="<?php echo $notification->getIdentity();?>">
     <div class="notification_item_photo">
     <?php if($notification->getContentObject() && ($notification->getContentObject() instanceof Core_Model_Item_Abstract)): ?>
      	<?php echo $this->htmlLink($notification->getContentObject()->getHref(),$this->itemPhoto($notification->getContentObject(), 'thumb.icon',$notification->getContentObject()->getTitle(),array("class"=>"notification_subject_icon"))); ?>
  	 <?php endif; ?>
     </div>
    <div class="notification_item_general notification_item_content notification_type_<?php echo $notification->type ?>">
      <?php echo $notification->__toString() ?>
    </div>
  </li>
<?php endforeach; ?>
