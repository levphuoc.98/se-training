<?php

class Book_Model_DbTable_Books extends Core_Model_Item_DbTable_Abstract
{
    protected $_rowClass = "Book_Model_Book";

    public function getBooksSelect($params = array())
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        $viewerId = $viewer->getIdentity();
        $table = Engine_Api::_()->getDbtable('books', 'book');
        $rName = $table->info('name');

        $select = $table->select()
            ->order(!empty($params['orderby']) ? $rName . '.' . $params['orderby'] . ' DESC' : $rName . '.creation_date DESC');

        if (!empty($params['user_id']) && is_numeric($params['user_id'])) {
            $owner = Engine_Api::_()->getItem('user', $params['user_id']);
            $select = $this->getProfileItemsSelect($owner, $select);
        } elseif (!empty($params['user']) && $params['user'] instanceof User_Model_User) {
            $owner = $params['user'];
            $select = $this->getProfileItemsSelect($owner, $select);
        } elseif (isset($params['users'])) {
            $str = (string)(is_array($params['users']) ? "'" . join("', '", $params['users']) . "'" : $params['users']);
            $select->where($rName . '.owner_id in (?)', new Zend_Db_Expr($str));
            $select->where("view_privacy != ? ", 'everyone');
        }

        return $select;
    }

    public function getBooksPaginator($params = array())
    {
        $paginator = Zend_Paginator::factory($this->getBooksSelect($params));
        if (!empty($params['page'])) {
            $paginator->setCurrentPageNumber($params['page']);
        }
        if (!empty($params['limit'])) {
            $paginator->setItemCountPerPage($params['limit']);
        }

        if (empty($params['limit'])) {
            $paginator->setItemCountPerPage(10);
        }

        return $paginator;
    }

    public function getArchiveList($spec)
    {
        if (!($spec instanceof User_Model_User)) {
            return null;
        }

        $localeObject = Zend_Registry::get('Locale');
        if (!$localeObject) {
            $localeObject = new Zend_Locale();
        }

        $dates = $this->select()
            ->from($this, 'creation_date')
            ->where('owner_type = ?', 'user')
            ->where('owner_id = ?', $spec->getIdentity())
            ->where('draft = ?', 0)
            ->order('blog_id DESC')
            ->query()
            ->fetchAll(Zend_Db::FETCH_COLUMN);

        $time = time();

        $archive_list = array();
        foreach ($dates as $date) {

            $date = strtotime($date);
            $ltime = localtime($date, true);
            $ltime["tm_mon"] = $ltime["tm_mon"] + 1;
            $ltime["tm_year"] = $ltime["tm_year"] + 1900;

            // LESS THAN A YEAR AGO - MONTHS
            if ($date + 31536000 > $time) {
                $date_start = mktime(0, 0, 0, $ltime["tm_mon"], 1, $ltime["tm_year"]);
                $date_end = mktime(0, 0, 0, $ltime["tm_mon"] + 1, 1, $ltime["tm_year"]);
                $type = 'month';

                $dateObject = new Zend_Date($date);
                $format = $localeObject->getTranslation('yMMMM', 'dateitem', $localeObject);
                $label = $dateObject->toString($format, $localeObject);
            } // MORE THAN A YEAR AGO - YEARS
            else {
                $date_start = mktime(0, 0, 0, 1, 1, $ltime["tm_year"]);
                $date_end = mktime(0, 0, 0, 1, 1, $ltime["tm_year"] + 1);
                $type = 'year';

                $dateObject = new Zend_Date($date);
                $format = $localeObject->getTranslation('yyyy', 'dateitem', $localeObject);
                if (!$format) {
                    $format = $localeObject->getTranslation('y', 'dateitem', $localeObject);
                }
                $label = $dateObject->toString($format, $localeObject);
            }

            if (!isset($archive_list[$date_start])) {
                $archive_list[$date_start] = array(
                    'type' => $type,
                    'label' => $label,
                    'date' => $date,
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                    'count' => 1
                );
            } else {
                $archive_list[$date_start]['count']++;
            }
        }

        return $archive_list;
    }
}
