DROP TABLE IF EXISTS `engine4_book_books`;
CREATE TABLE `engine4_book_books`
(
    `book_id`       int(11) unsigned NOT NULL auto_increment,
    `book_type`     varchar(64)      NOT NULL,
    `title`         varchar(128)     NOT NULL,
    `owner_id`      int(11) unsigned NOT NULL,
    `owner_type`    int(11) unsigned NOT NULL default '0',
    `photo_id`      int(11) unsigned NOT NULL default '0',
    `creation_date` datetime         NOT NULL,
    `modified_date` datetime         NOT NULL,
    `view_privacy`  VARCHAR(24)      NOT NULL default 'everyone',
    PRIMARY KEY (`book_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE utf8_unicode_ci;


INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`)
VALUES ('book_main', 'standard', 'Book Main Navigation Menu');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`)
VALUES ('core_main_book', 'book', 'Books', '', '{"route":"book_general","icon":"","enabled":"1"}', 'core_main', '', 10),
       ('book_main_browse', 'book', 'Browse Entries', '', '{"route":"book_general","icon":"fa fa-search","target":""}', 'book_main', '', 1),
       ('book_main_manage', 'book', 'My Books', '', '{"route":"book_general","action"="manager","icon":"fa fa-user","target":""}', 'book_main', '', 2),
       ('book_main_create', 'book', 'New Book', '', '{"route":"book_general","action"="create","icon":"fa fa-pencil-alt","target":""}', 'book_main', '', 3);