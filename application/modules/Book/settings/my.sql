INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('book', 'Books', 'Books', '5.4.1', 1, 'extra') ;

DROP TABLE IF EXISTS `engine4_book_books`;
CREATE TABLE `engine4_book_books`
(
    `book_id`       int(11) unsigned NOT NULL auto_increment,
    `title`         varchar(128)     NOT NULL,
    `content`       varchar(256)     NOT NULL,
    `photo_id`      int(11) unsigned NOT NULL,
    `owner_id`      int(11) unsigned NOT NULL,
    `owner_type`    varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `view_count`    int(11) unsigned NOT NULL default '0',
    `comment_count` int(11) unsigned NOT NULL default '0',
    `like_count`    int(11) unsigned NOT NULL default '0',
    `feature`       bool             not null default false,
    `creation_date` datetime         NOT NULL,
    `modified_date` datetime         NOT NULL,
    `view_privacy`  VARCHAR(24)      NOT NULL default 'everyone',
    PRIMARY KEY (`book_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE utf8_unicode_ci;


INSERT INTO `engine4_core_pages` (`name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`, `fragment`, `layout`, `levels`, `provides`, `view_count`) VALUES
('book_index_create', 'Book Create Page', NULL, 'Book Create Page', '', '', 0, 0, '', NULL, 'no-subject', 0);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`) VALUES
((SELECT page.page_id FROM engine4_core_pages  page WHERE page.NAME = 'book_index_create'), 'container', 'main', NULL, 1, '[""]', NULL);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
SELECT page.page_id, 'container', 'middle', content.content_id, 1, '[""]', NULL
FROM engine4_core_pages page JOIN engine4_core_content content ON page.page_id = content.page_id
WHERE page.NAME='book_index_create' AND content.NAME='main';

INSERT INTO `engine4_core_pages` (`name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`,`fragment`, `layout`, `levels`, `provides`, `view_count`)
VALUES ('book_index_view', 'Book View Page', NULL, 'Book View Page', '', '', 0, 0, '', NULL, 'no-subject', 0);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
VALUES ((SELECT page.page_id FROM engine4_core_pages page WHERE page.NAME = 'book_index_view'), 'container', 'main',NULL, 1, '[""]', NULL);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
SELECT page.page_id, 'container', 'middle', content.content_id, 1, '[""]', NULL
FROM engine4_core_pages page JOIN engine4_core_content content ON page.page_id = content.page_id
WHERE page.NAME = 'book_index_view' AND content.NAME = 'main';

INSERT INTO `engine4_core_pages` (`name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`,`fragment`, `layout`, `levels`, `provides`, `view_count`)
VALUES ('book_index_manage', 'My Book', NULL, 'My Book', '', '', 0, 0, '', NULL, 'no-subject', 0);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
VALUES ((SELECT page.page_id FROM engine4_core_pages page WHERE page.NAME = 'book_index_manage'), 'container', 'main',NULL, 1, '[""]', NULL);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
SELECT page.page_id, 'container', 'middle', content.content_id, 1, '[""]', NULL
FROM engine4_core_pages page JOIN engine4_core_content content ON page.page_id = content.page_id
WHERE page.NAME = 'book_index_manage' AND content.NAME = 'main';

INSERT INTO `engine4_core_pages` (`name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`,`fragment`, `layout`, `levels`, `provides`, `view_count`)
VALUES ('book_index_list', 'Book List Page', NULL, 'Book View Page', '', '', 0, 0, '', NULL, 'no-subject', 0);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
VALUES ((SELECT page.page_id FROM engine4_core_pages page WHERE page.NAME = 'book_index_list'), 'container', 'main',NULL, 1, '[""]', NULL);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
SELECT page.page_id, 'container', 'middle', content.content_id, 1, '[""]', NULL
FROM engine4_core_pages page JOIN engine4_core_content content ON page.page_id = content.page_id
WHERE page.NAME = 'book_index_list' AND content.NAME = 'main';

INSERT INTO `engine4_core_pages` (`name`, `displayname`, `url`, `title`, `description`, `keywords`, `custom`,`fragment`, `layout`, `levels`, `provides`, `view_count`)
VALUES ('book_index_index', 'Book Index Page', NULL, 'Book Index Page', '', '', 0, 0, '', NULL, 'no-subject', 0);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
VALUES ((SELECT page.page_id FROM engine4_core_pages page WHERE page.NAME = 'book_index_index'), 'container', 'main',NULL, 1, '[""]', NULL);
INSERT INTO `engine4_core_content` (`page_id`, `type`, `name`, `parent_content_id`, `order`, `params`, `attribs`)
SELECT page.page_id, 'container', 'middle', content.content_id, 1, '[""]', NULL
FROM engine4_core_pages page JOIN engine4_core_content content ON page.page_id = content.page_id
WHERE page.NAME = 'book_index_index' AND content.NAME = 'main';


INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`)
VALUES ('book_main', 'standard', 'Book Main Navigation Menu'),
       ('book_gutter', 'standard', 'Book Gutter Navigation Menu');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`)
VALUES ('core_main_book', 'book', 'Books', '', '{"route":"book_general","icon":"","enabled":"1"}', 'core_main', '', 10),
       ('book_main_browse', 'book', 'Browse Entries', '', '{"route":"book_general","icon":"fa fa-search","target":""}', 'book_main', '', 1),
       ('book_main_manage', 'book', 'My Books', '', '{"route":"book_general","action":"manager","icon":"fa fa-user","target":""}', 'book_main', '', 2),
       ('book_main_create', 'book', 'New Book', '', '{"route":"book_general","action":"create","icon":"fa fa-pencil-alt","target":""}', 'book_main', '', 3),
        ('book_gutter_list', 'book', 'View All Entries', 'Book_Plugin_Menus', '{"route":"book_general","action":"index","class":"buttonlink icon_blog_viewall"}', 'book_gutter', '', 1),
        ('book_gutter_create', 'book', 'Write New Entry', 'Book_Plugin_Menus', '{"route":"book_general","action":"create","class":"buttonlink icon_blog_new"}', 'book_gutter', '', 2),
        ('book_gutter_edit', 'book', 'Edit This Entry', 'Book_Plugin_Menus', '{"route":"book_specific","action":"edit","class":"buttonlink icon_blog_edit"}', 'book_gutter', '', 3),
        ('book_gutter_delete', 'book', 'Delete This Entry', 'Book_Plugin_Menus', '{"route":"book_specific","action":"delete","class":"buttonlink smoothbox icon_blog_delete"}', 'book_gutter', '', 4),
        ('book_gutter_share', 'book', 'Share', 'Book_Plugin_Menus', '{"route":"default","module":"activity","controller":"index","action":"share","class":"buttonlink smoothbox icon_comments"}', 'book_gutter', '', 5),
        ('book_gutter_contact', 'book', 'Contact Owner', 'Book_Plugin_Menus', '{"route":"default","module":"activity","controller":"index","action":"share","class":"buttonlink smoothbox icon_comments"}', 'book_gutter', '', 5);



INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`) VALUES
('book_new', 'book', '{item:$subject} wrote a new book entry:', 1, 5, 1, 3, 1, 1),
('book_comment', 'book', '{item:$subject} commented on {item:$owner}''s {item:$object:book entry}.', 1, 1, 1, 3, 3, 0),
('book_like', 'book', '{item:$subject} liked {item:$owner}''s {item:$object:book entry}.', 1, 1, 1, 3, 3, 0);
