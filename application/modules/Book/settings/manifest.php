<?php return array(
    'package' =>
        array(
            'type' => 'module',
            'name' => 'book',
            'version' => '5.4.1',
            'path' => 'application/modules/Book',
            'title' => 'Books',
            'description' => 'Books',
            'author' => 'phuoc',
            'callback' =>
                array(
                    'class' => 'Engine_Package_Installer_Module',
                ),
            'actions' =>
                array(
                    0 => 'install',
                    1 => 'upgrade',
                    2 => 'refresh',
                    3 => 'enable',
                    4 => 'disable',
                ),
            'directories' =>
                array(
                    0 => 'application/modules/Book',
                ),
            'files' =>
                array(
                    0 => 'application/languages/en/book.csv',
                ),
        ),
    'items' => array(
        'book',
    ),
    'routes' => array(
        'book_general' => array(
            'route' => 'books/:action/*',
            'defaults' => array(
                'module' => 'book',
                'controller' => 'index',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '(index|manage|create|edit|delete|upload-photo)',
            ),
        ),
        'book_specific' => array(
            'route' => 'books/:action/:book_id/*',
            'defaults' => array(
                'module' => 'book',
                'controller' => 'index',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '(edit|delete)',
                'book_id' => '\d+'
            ),
        ),
        'book_view_user' => array(
            'route' => 'books/:user_id/*',
            'defaults' => array(
                'module' => 'book',
                'controller' => 'index',
                'action' => 'list',
            ),
            'reqs' => array(
                'user_id' => '\d+',
            ),
        ),
        'book_entry_view' => array(
            'route' => 'books/:user_id/:book_id/:slug',
            'defaults' => array(
                'module' => 'book',
                'controller' => 'index',
                'action' => 'view',
                'slug' => '',
            ),
            'reqs' => array(
                'user_id' => '\d+',
                'book_id' => '\d+'
            ),
        ),
    ),
); ?>