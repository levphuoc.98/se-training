<?php
return array(
    array(
        'title' => 'Popular Book Entries',
        'description' => 'Displays a list of most viewed book entries.',
        'category' => 'Books',
        'type' => 'widget',
        'name' => 'book.list-popular-books',
        'isPaginated' => true,
        'defaultParams' => array(
            'title' => 'Popular Book Entries',
        ),
        'requirements' => array(
            'no-subject',
        ),
        'adminForm' => array(
            'elements' => array(
                array(
                    'Radio',
                    'popularType',
                    array(
                        'label' => 'Popular Type',
                        'multiOptions' => array(
                            'view' => 'Views',
                            'comment' => 'Comments',
                        )
                    )
                ),
            )
        ),
    ),
    array(
        'title' => 'Book Gutter Menu',
        'description' => 'Displays a menu in the book gutter.',
        'category' => 'Books',
        'type' => 'widget',
        'name' => 'book.gutter-menu',
    ),
) ?>
