<?php

class Book_IndexController extends Core_Controller_Action_Standard
{
    public function init()
    {
        if (!$this->_helper->requireAuth()->setAuthParams('book', null, 'view')->isValid()) return;
    }

    public function indexAction()
    {
        // Prepare data
        $viewer = Engine_Api::_()->user()->getViewer();

        $values['page'] = 1;
        $values['limit'] = 10;

        $paginator = Engine_Api::_()->getItemTable('book')->getBooksPaginator($values);

        $paginator->setItemCountPerPage($values['limit']);

        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);

        $this->view->viewer = $viewer;
        // Render
        $this->_helper->content->setEnabled();
    }

    public function manageAction()
    {
        if (!$this->_helper->requireUser()->isValid()) return;
        $viewer = Engine_Api::_()->user()->getViewer();

        $values['user_id'] = $viewer->getIdentity();
        $values['page'] = 1;
        $values['limit'] = 10;
        // Get paginator
        $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('book')->getBooksPaginator($values);
        $paginator->setItemCountPerPage($values['limit']);
        $this->view->paginator = $paginator->setCurrentPageNumber($values['page']);
        $this->_helper->content->setEnabled();
    }

    public function createAction()
    {
        if (!$this->_helper->requireUser()->isValid()) return;
        // Render
        $this->_helper->content->setEnabled();

        $viewer = Engine_Api::_()->user()->getViewer();
        $values['user_id'] = $viewer->getIdentity();
        $paginator = Engine_Api::_()->getItemTable('book')->getBooksPaginator($values);

        $this->view->quota = $quota = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'book', 'max');
        $this->view->current_count = $paginator->getTotalItemCount();

        // Prepare form
        $this->view->form = $form = new Book_Form_Create();

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }
        // Process
        $table = Engine_Api::_()->getItemTable('book');
        $db = $table->getAdapter();
        $db->beginTransaction();

        try {
            $viewer = Engine_Api::_()->user()->getViewer();
            $formValues = $form->getValues();
            $formValues['auth_view'] = 'everyone';
            $values = array_merge($formValues, array(
                'owner_id' => $viewer->getIdentity(),
                'view_privacy' => $formValues['auth_view'],
            ));
            $book = $table->createRow();
            $book->setFromArray($values);
            // Commit
            $book->save();
            if (!empty($values['photo'])) {
                $book->setPhoto($form->photo);
            }

            $auth = Engine_Api::_()->authorization()->context;

            $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');

            $viewMax = array_search($values['auth_view'], $roles);
            $commentMax = array_search($values['auth_comment'], $roles);

            foreach ($roles as $i => $role) {
                $auth->setAllowed($book, $role, 'view', ($i <= $viewMax));
                $auth->setAllowed($book, $role, 'comment', ($i <= $commentMax));
            }

            $db->commit();
        } catch (Exception $e) {
            return $this->exceptionWrapper($e, $form, $db);
        }

        return $this->_helper->redirector->gotoRoute(array('action' => 'manage'));
    }

    public function editAction()
    {
        if (!$this->_helper->requireUser()->isValid()) return;

        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->getItem('book', $this->_getParam('book_id'));
        if (!Engine_Api::_()->core()->hasSubject('book')) {
            Engine_Api::_()->core()->setSubject($book);
        }

        if (!$this->_helper->requireSubject()->isValid()) return;

        // Get navigation
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('book_main');

//        if( !$this->_helper->requireAuth()->setAuthParams($book, $viewer, 'edit')->isValid() ) {
//            return;
//        }

        // Prepare form
        $this->view->form = $form = new Book_Form_Edit();

        // Populate form
        $form->populate($book->toArray());


        $auth = Engine_Api::_()->authorization()->context;
        // Check post/form
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Process
        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();

        try {
            $values = $form->getValues();
            $values['view_privacy'] = $values['auth_view'];

            $book->setFromArray($values);
            $book->modified_date = date('Y-m-d H:i:s');
            $book->save();

            // Add photo
            if (!empty($values['photo'])) {
                $book->setPhoto($form->photo);
            }

            // insert new activity if blog is just getting published
//            $action = Engine_Api::_()->getDbtable('actions', 'activity')->getActionsByObject($blog);
//            if (count($action->toArray()) <= 0 && $values['draft'] == '0') {
//                $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($viewer, $blog, 'blog_new', '', array('privacy' => isset($values['networks']) ? $network_privacy : null));
//                // make sure action exists before attaching the blog to the activity
//                if ($action != null) {
//                    Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $blog);
//                }
//            }

            // Rebuild privacy
//            $actionTable = Engine_Api::_()->getDbtable('actions', 'activity');
//            foreach ($actionTable->getActionsByObject($blog) as $action) {
//                $action->privacy = isset($values['networks']) ? $network_privacy : null;
//                $action->save();
//                $actionTable->resetActivityBindings($action);
//            }

            // Send notifications for subscribers
//            Engine_Api::_()->getDbtable('subscriptions', 'book')->sendNotifications($book);

            $db->commit();

        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return $this->_helper->redirector->gotoRoute(array('action' => 'manage'));
    }

    public function deleteAction()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->getItem('book', $this->getRequest()->getParam('book_id'));
//        if( !$this->_helper->requireAuth()->setAuthParams($book, null, 'delete')->isValid()) return;

        // In smoothbox
        $this->_helper->layout->setLayout('default-simple');

        $this->view->form = $form = new Book_Form_Delete();

        if (!$book) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_("Book entry doesn't exist or not authorized to delete");
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }

        $db = $book->getTable()->getAdapter();
        $db->beginTransaction();

        try {
            $book->delete();
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('Your Book has been deleted.');
        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'manage'), 'book_general', true),
            'messages' => array($this->view->message)
        ));
    }

    public function viewAction()
    {
        // Check permission
        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->getItem('book', $this->_getParam('book_id'));
        if ($book) {
            Engine_Api::_()->core()->setSubject($book);
        }

        if (!$this->_helper->requireSubject()->isValid()) {
            return;
        }
        if (!$this->_helper->requireAuth()->setAuthParams($book, $viewer, 'view')->isValid()) {
            return;
        }
        if (!$book || !$book->getIdentity()) {
            return $this->_helper->requireSubject->forward();
        }
        $bookTable = Engine_Api::_()->getDbtable('books', 'book');
        // Prepare data
        $this->view->book = $book;
        $this->view->owner = $owner = $book->getOwner();
        $this->view->viewer = $viewer;
        // Render
        if (!$book->isOwner($viewer)) {
            $bookTable->update(array(
                'view_count' => new Zend_Db_Expr('view_count + 1'),
            ), array(
                'book_id = ?' => $book->getIdentity(),
            ));
        }
        $this->_helper->content->setEnabled();
    }

    public function uploadPhotoAction()
    {
        $viewer = Engine_Api::_()->user()->getViewer();

        $this->_helper->layout->disableLayout();

        if (!Engine_Api::_()->authorization()->isAllowed('album', $viewer, 'create')) {
            return false;
        }

        if (!$this->_helper->requireAuth()->setAuthParams('album', null, 'create')->isValid()) return;

        if (!$this->_helper->requireUser()->checkRequire()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Max file size limit exceeded (probably).');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }
        if (!isset($_FILES['userfile']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid Upload');
            return;
        }

        $db = Engine_Api::_()->getDbtable('photos', 'album')->getAdapter();
        $db->beginTransaction();

        try {
            $viewer = Engine_Api::_()->user()->getViewer();

            $photoTable = Engine_Api::_()->getDbtable('photos', 'album');
            $photo = $photoTable->createRow();
            $photo->setFromArray(array(
                'owner_type' => 'user',
                'owner_id' => $viewer->getIdentity()
            ));
            $photo->save();

            $photo->setPhoto($_FILES['userfile']);

            $this->view->status = true;
            $this->view->name = $_FILES['userfile']['name'];
            $this->view->photo_id = $photo->photo_id;
            $this->view->photo_url = $photo->getPhotoUrl();

            $table = Engine_Api::_()->getDbtable('albums', 'album');
            $album = $table->getSpecialAlbum($viewer, 'book');

            $photo->album_id = $album->album_id;
            $photo->save();

            if (!$album->photo_id) {
                $album->photo_id = $photo->getIdentity();
                $album->save();
            }

            $auth = Engine_Api::_()->authorization()->context;
            $auth->setAllowed($photo, 'everyone', 'view', true);
            $auth->setAllowed($photo, 'everyone', 'comment', true);
            $auth->setAllowed($album, 'everyone', 'view', true);
            $auth->setAllowed($album, 'everyone', 'comment', true);

            $photo->order = $photo->photo_id;
            $photo->save();

            $db->commit();

        } catch (Album_Model_Exception $e) {
            $db->rollBack();
            $this->view->status = false;
            $this->view->error = $this->view->translate($e->getMessage());
            throw $e;
            return;

        } catch (Exception $e) {
            $db->rollBack();
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('An error occurred.');
            throw $e;
            return;
        }
    }

    public function makeFeatureAction()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->getItem('book', $this->getRequest()->getParam('book_id'));

        // In smoothbox
        $this->_helper->layout->setLayout('default-simple');

        $this->view->form = $form = new Book_Form_MakeFeature();

        if (!$book) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_("Book entry doesn't exist or not authorized to make feature");
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->view->status = false;
            $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid request method');
            return;
        }

        $db = $book->getTable()->getAdapter();
        $db->beginTransaction();

        try {
            $book->__set('feature', true);
            $book->__set('modified_date', date('Y-m-d H:i:s'));
            $book->save();
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        $notifyApi = Engine_Api::_()->getDbtable('notifications', 'activity');
        $notifyApi->addNotification($book->getOwner(), $viewer, $book, 'make_feature');

        $this->view->status = true;
        $this->view->message = Zend_Registry::get('Zend_Translate')->_('This Book has been made feature!');
        return $this->_forward('success', 'utility', 'core', array(
            'parentRedirect' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index'), 'book_general', true),
            'messages' => array($this->view->message)
        ));
    }

    public function listAction()
    {
        $this->view->someVar = 'someVal';
    }
}
