<?php
class Book_Widget_GutterMenuController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
    if( Engine_Api::_()->core()->hasSubject('book') ) {
      $this->view->book = $book = Engine_Api::_()->core()->getSubject('book');
      $this->view->owner = $owner = $book->getOwner();
    } else if( Engine_Api::_()->core()->hasSubject('user') ) {
      $this->view->book = null;
      $this->view->owner = $owner = Engine_Api::_()->core()->getSubject('user');
    } else {
      return $this->setNoRender();
    }
    
    // Get navigation
    $this->view->gutterNavigation = $gutterNavigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('book_gutter');
  }
}
