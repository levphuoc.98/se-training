<?php

class Book_Form_Create extends Engine_Form
{
    public $_error = array();

    public function init()
    {
        $this->setTitle('Post New Book')
            ->setDescription('Compose your new book entry below, then click "Post Entry" to publish the entry to your book.')
            ->setAttrib('name', 'books_create');
        $user = Engine_Api::_()->user()->getViewer();

        $this->addElement('Text', 'title', array(
            'label' => 'Book Title',
            'allowEmpty' => false,
            'required' => true,
            'maxlength' => '63',
            'filters' => array(
                new Engine_Filter_Censor(),
                'StripTags',
                new Engine_Filter_StringLength(array('max' => '63'))
            ),
            'autofocus' => 'autofocus',
        ));

        $this->addElement('Text', 'content', array(
            'label' => 'Book Content',
            'allowEmpty' => false,
            'required' => true,
            'maxlength' => '255',
            'filters' => array(
                new Engine_Filter_Censor(),
                'StripTags',
                new Engine_Filter_StringLength(array('max' => '255'))
            ),
            'autofocus' => 'autofocus',
        ));
        $this->addElement('File', 'photo', array(
            'label' => 'Choose Book Photo',
        ));
        $this->photo->addValidator('Extension', false, 'jpg,png,gif,jpeg');

        // Element: auth_view
        $viewOptions = (array)Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('books', $user, 'auth_view');
        // Element: auth_comment
        $commentOptions = (array)Engine_Api::_()->authorization()->getAdapter('levels')->getAllowed('books', $user, 'auth_comment');

        $availableLabels = array(
            'everyone' => 'Everyone',
            'registered' => 'All Registered Members',
            'owner_network' => 'Friends and Networks',
            'owner_member_member' => 'Friends of Friends',
            'owner_member' => 'Friends Only',
            'owner' => 'Just Me'
        );
        $viewOptions = (!empty($viewOptions)) ? array_intersect_key($availableLabels, array_flip($viewOptions)) : $availableLabels;
        $commentOptions = (!empty($commentOptions)) ? array_intersect_key($availableLabels, array_flip($commentOptions)) : $availableLabels;


        if (!empty($viewOptions) && count($viewOptions) >= 1) {
            // Make a hidden field
            if (count($viewOptions) == 1) {
                $this->addElement('hidden', 'auth_view', array('order' => 101, 'value' => key($viewOptions)));
                // Make select box
            } else {
                $this->addElement('Select', 'auth_view', array(
                    'label' => 'Privacy',
                    'description' => 'Who may see this book entry?',
                    'multiOptions' => $viewOptions,
                    'value' => key($viewOptions),
                ));
                $this->auth_view->getDecorator('Description')->setOption('placement', 'append');
            }
        }

        if (!empty($commentOptions) && count($commentOptions) >= 1) {
            // Make a hidden field
            if (count($commentOptions) == 1) {
                $this->addElement('hidden', 'auth_comment', array('order' => 102, 'value' => key($commentOptions)));
                // Make select box
            } else {
                $this->addElement('Select', 'auth_comment', array(
                    'label' => 'Comment Privacy',
                    'description' => 'Who may post comments on this book entry?',
                    'multiOptions' => $commentOptions,
                    'value' => key($commentOptions),
                ));
                $this->auth_comment->getDecorator('Description')->setOption('placement', 'append');
            }
        }

        $this->addElement('Hash', 'token', array(
            'timeout' => 3600,
        ));

        $this->addElement('Button', 'submit', array(
            'label' => 'Post Entry',
            'type' => 'submit',
        ));
    }

    public function postEntry()
    {
        $values = $this->getValues();
        $user = Engine_Api::_()->user()->getViewer();
        $title = $values['title'];
        $content = $values['content'];

        $db = Engine_Db_Table::getDefaultAdapter();
        $db->beginTransaction();
        try {
            // Transaction
            $table = Engine_Api::_()->getDbtable('books', 'books');
            $row = $table->createRow();
            $row->owner_id = $user->getIdentity();
            $row->creation_date = date('Y-m-d H:i:s');
            $row->modified_date = date('Y-m-d H:i:s');
            $row->title = $title;
            $row->content = $content;
            $row->save();

            $bookId = $row->book_id;

            $attachment = Engine_Api::_()->getItem($row->getType(), $bookId);
            $action = Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $row, 'book_new');
            Engine_Api::_()->getDbtable('actions', 'activity')->attachActivity($action, $attachment);
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }
}
