<?php

class Book_Form_Edit extends Book_Form_Create
{
    public function init()
    {
        parent::init();
        $this->setTitle('Edit Book')
            ->setDescription('Edit your entry below, then click "Post Entry" to publish the entry on your Book.');
        $this->submit->setLabel('Save Changes');
    }
}
