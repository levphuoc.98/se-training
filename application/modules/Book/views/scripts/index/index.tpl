<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
<ul class="blogs_browse">
    <?php foreach( $this->paginator as $item ): ?>
    <li>
        <div class='blogs_browse_photo'>
            <?php echo $this->htmlLink($item->getHref(), $this->itemBackgroundPhoto($item, 'thumb.main')) ?>
        </div>
        <div class='blogs_browse_info'>
          <span class='blogs_browse_info_title'>
            <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?></h3>
          </span>
            <p class='blogs_browse_info_date'>
                <?php echo $this->translate('Posted');?>
                <?php echo $this->timestamp(strtotime($item->creation_date)) ?>
                <?php echo $this->translate('by');?>
                <?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>
            </p>
            <p class='blogs_browse_info_blurb'>
                <?php $readMore = ' ' . $this->translate('Read More') . '...';?>
                <?php echo $this->string()->truncate($this->string()->stripTags($item->content), 110, $this->htmlLink($item->getHref(), $readMore) ) ?>
            </p>
        </div>
        <?php if (($this->viewer->level_id == 1 || $this->viewer->level_id == 2) && ($item->feature == false)): ?>
            <?= $this->htmlLink(array('route' => 'default', 'module' => 'book', 'controller' => 'index', 'action' => 'make-feature', 'book_id' => $item->getIdentity()), "Make Future", array('class' => 'smoothbox')) ?>
        <?php elseif ($item->feature == true): ?>
            <?= $this->translate('Featured') ?>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php else:?>
<div class="tip">
    <span>
      <?php echo $this->translate('Nobody has post a book entry yet.'); ?>
        <?php if( $this->canCreate ): ?>
        <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('action' => 'create'), 'book_general').'">', '</a>'); ?>
        <?php endif; ?>
    </span>
</div>
<?php endif; ?>

<?php echo $this->paginationControl($this->paginator, null, null, array(
'pageAsQuery' => true,
'query' => $this->formValues,
//'params' => $this->formValues,
)); ?>
