<div class="layout_middle">
    <div class="generic_layout_container">
        <div class="headline">
            <h2>
                <?php echo $this->translate('Book');?>
            </h2>
            <div class="tabs">
                <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render(); ?>
            </div>
        </div>
    </div>
</div>
<div class="layout_middle">
    <div class="generic_layout_container">
        <?php echo $this->form->render($this) ?>
    </div>
</div>

<script type="text/javascript">
    $$('.core_main_book').getParent().addClass('active');
</script>
