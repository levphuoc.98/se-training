<h2>
    <?php echo $this->htmlLink(array('route' => 'book_general'), "Books", array()); ?>
    <?php echo $this->translate('&#187;'); ?>
</h2>
<h2>
    <?php echo $this->book->getTitle(); ?>
</h2>
<ul class='blogs_entrylist'>
    <li>
        <div class="blog_entrylist_entry_date">
            <?php echo $this->translate('Posted by');?>
            <?php echo $this->htmlLink($this->book->getOwner()->getHref(), $this->book->getOwner()->getTitle()) ?>
            <?php echo $this->timestamp($this->book->creation_date) ?>
            -
            <?php echo $this->translate(array('%s view', '%s views', $this->book->view_count), $this->locale()->toNumber($this->book->view_count)) ?>
        </div>
        <h4><?= $this->translate('Content') ?></h4>
        <div class="blog_entrylist_entry_body rich_content_body">
            <?php echo Engine_Api::_()->core()->smileyToEmoticons($this->book->content); ?>
        </div>
        <h4><?= $this->translate('Image') ?></h4>
        <img src="<?= $this->book->getPhotoUrl() ?>"> <br>
        <?php if ($this->viewer->getIdentity() != $this->book->getOwner()->getIdentity()): ?>
        <?= $this->htmlLink(array('route' => 'messages_general', 'action' =>'compose', 'to' => $this->book->getOwner()->getIdentity()),
                    "Contact Owner", array('class' => 'smoothbox')) ?>
        <?php endif; ?>
    </li>
</ul>
<script type="text/javascript">
    $$('.core_main_book').getParent().addClass('active');
</script>