<script type="text/javascript">
    var pageAction = function (page) {
        $('page').value = page;
        $('filter_form').submit();
    }
</script>
<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
<ul class="books_manage list_wrapper">
    <?php foreach( $this->paginator as $item ): ?>
    <li>
        <div class='blogs_photo'>
            <?php echo $this->htmlLink($item->getHref(), $this->itemBackgroundPhoto($item, '')) ?>
        </div>
        <div class='blogs_options'>
            <?php echo $this->htmlLink(array(
            'route' => 'book_specific',
            'module' => 'book',
            'controller' => 'index',
            'action' => 'edit',
            'book_id' => $item->getIdentity(),
            'reset' => true,
            ), $this->translate('Edit Entry'), array(
            'class' => 'buttonlink icon_blog_edit',
            )) ?>
            <?php echo $this->htmlLink(array(
            'route' => 'book_specific',
            'module' => 'book',
            'controller' => 'index',
            'action' => 'delete',
            'book_id' => $item->getIdentity(),
            'format' => 'smoothbox'
            ), $this->translate('Delete Entry'), array(
            'class' => 'buttonlink smoothbox icon_blog_delete'
            )); ?>
        </div>
        <div class='blogs_info'>
            <span class='blogs_browse_info_title'>
                <h3><?php echo $this->htmlLink($item->getHref(), $item->getTitle()) ?></h3>
            </span>
            <p class='blogs_browse_info_date'>
                <?php echo $this->translate('Content');?>
                <?php echo $this->translate($item->getContent()) ?>
                <?php echo $this->translate('about');?>
                <?php echo $this->timestamp(strtotime($item->creation_date)) ?>
            </p>
            <p class='blogs_browse_info_date'>
                <?php echo $this->translate('Posted by');?>
                <?php echo $this->htmlLink($item->getOwner()->getHref(), $item->getOwner()->getTitle()) ?>
                <?php echo $this->translate('about');?>
                <?php echo $this->timestamp(strtotime($item->creation_date)) ?>
            </p>
        </div>
    </li>
    <?php endforeach; ?>
</ul>

<?php elseif($this->search): ?>
<div class="tip">
      <span>
        <?php echo $this->translate('You do not have any book entries that match your search criteria.');?>
      </span>
</div>
<?php else: ?>
<div class="tip">
      <span>
          <?php echo $this->translate('You do not have any book entries.');?>
          <?php echo $this->translate('Get started by %1$swriting%2$s a new entry.', '<a href="'.$this->url(array('action' => 'create'), 'book_general').'">', '</a>'); ?>
      </span>
</div>
<?php endif; ?>

<?php echo $this->paginationControl($this->paginator, null, null, array('pageAsQuery' => true,'query' => $this->formValues,'params' => $this->formValues,)); ?>


<script type="text/javascript">
    $$('.core_main_book').getParent().addClass('active');
</script>
