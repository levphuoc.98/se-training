<?php

class Book_Plugin_Menus
{
    public function canCreateBooks()
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$viewer || !$viewer->getIdentity()) {
            return false;
        }
        if (!Engine_Api::_()->authorization()->isAllowed('book', $viewer, 'create')) {
            return false;
        }
        return true;
    }

    public function canViewBooks()
    {
        $viewer = Engine_Api::_()->user()->getViewer();

        if (!Engine_Api::_()->authorization()->isAllowed('book', $viewer, 'view')) {
            return false;
        }

        return true;
    }

    public function onMenuInitialize_BookGutterList($row)
    {
        if (!Engine_Api::_()->core()->hasSubject()) {
            return false;
        }

        $subject = Engine_Api::_()->core()->getSubject();
        if ($subject instanceof User_Model_User) {
            $user_id = $subject->getIdentity();
        } else if ($subject instanceof Blog_Model_Blog) {
            $user_id = $subject->owner_id;
        } else {
            return false;
        }

// Modify params
        $params = $row->params;
        $params['params']['user_id'] = $user_id;
        return $params;
    }

    public function onMenuInitialize_BookGutterShare($row)
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$viewer->getIdentity()) {
            return false;
        }

        if (!Engine_Api::_()->core()->hasSubject()) {
            return false;
        }

        $subject = Engine_Api::_()->core()->getSubject();
        if (!($subject instanceof Book_Model_Book)) {
            return false;
        }

// Modify params
        $params = $row->params;
        $params['params']['type'] = $subject->getType();
        $params['params']['id'] = $subject->getIdentity();
        $params['params']['format'] = 'smoothbox';
        return $params;
    }

    public function onMenuInitialize_BookGutterCreate($row)
    {
        $viewer = Engine_Api::_()->user()->getViewer();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $owner = Engine_Api::_()->getItem('user', $request->getParam('user_id'));

        if ($viewer->getIdentity() != $owner->getIdentity()) {
            return false;
        }

        if (!Engine_Api::_()->authorization()->isAllowed('book', $viewer, 'create')) {
            return false;
        }

        return true;
    }

    public function onMenuInitialize_BookGutterEdit($row)
    {
        if (!Engine_Api::_()->core()->hasSubject()) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->core()->getSubject('book');

        if (!$book->authorization()->isAllowed($viewer, 'edit')) {
            return false;
        }

// Modify params
        $params = $row->params;
        $params['params']['book_id'] = $book->getIdentity();
        return $params;
    }

    public function onMenuInitialize_BookGutterDelete($row)
    {
        if (!Engine_Api::_()->core()->hasSubject()) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $book = Engine_Api::_()->core()->getSubject('blog');

        if (!$book->authorization()->isAllowed($viewer, 'delete')) {
            return false;
        }

// Modify params
        $params = $row->params;
        $params['params']['book_id'] = $book->getIdentity();
        return $params;
    }
}