<?php
return array(
  'package' => array(
    'type' => 'library',
    'name' => 'gif',
    'version' => '5.4.1',
    'revision' => '$Revision: 10171 $',
    'path' => 'application/libraries/GIF',
    'repository' => 'socialengine.com',
    'title' => 'GIF',
    'author' => 'Webligo Developments',
    'directories' => array(
      'application/libraries/GIF',
    )
  )
  )
?>
