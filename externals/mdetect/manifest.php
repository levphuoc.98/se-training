<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'mdetect',
    'version' => '5.4.1',
    'revision' => '$Revision: 7593 $',
    'path' => 'externals/mdetect',
    'repository' => 'socialengine.com',
    'title' => 'mDetect',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/mdetect',
    )
  )
) ?>
