<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'tagger',
    'version' => '5.4.1',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/tagger',
    'repository' => 'socialengine.com',
    'title' => 'Tagger',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/tagger',
    )
  )
) ?>
