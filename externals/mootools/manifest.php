<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'mootools',
    'version' => '5.4.1',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/mootools',
    'repository' => 'socialengine.com',
    'title' => 'Mootools',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/mootools',
    )
  )
) ?>
